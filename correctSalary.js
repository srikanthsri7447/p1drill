const data = require('./data')


function salaryCorrection(data){

    const correctSalaryArray = data.map((eachEmployee)=>{
    
        let salary = Number(eachEmployee['salary'].replace('$',''))
        eachEmployee['correctSalary'] = Number(salary*10000)
        return eachEmployee
    })
    return correctSalaryArray;
}


module.exports = salaryCorrection;