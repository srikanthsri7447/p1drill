const data = require('./data')

let sum = data.reduce((accumulator, curValue) => {

    const salary = curValue.salary.split('$')

    return accumulator + Number(salary[1])

}, 0)


console.log(sum)