const data = require('./data')

const convertedSalary = data.map((eachEmployee)=>{
    
    let salary = eachEmployee['salary'].replace('$', '')
    eachEmployee['salary'] = Number(salary)
    return eachEmployee
})

console.log(convertedSalary)